<?php
/**
 * Cleaner walker for wp_nav_menu()
 *
 * Walker_Nav_Menu (WordPress default) example output:
 *   <li id="menu-item-8" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8"><a href="/">Home</a></li>
 *   <li id="menu-item-9" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-9"><a href="/sample-page/">Sample Page</a></l
 *
 * Roots_Nav_Walker example output:
 *   <li class="menu-home"><a href="/">Home</a></li>
 *   <li class="menu-sample-page"><a href="/sample-page/">Sample Page</a></li>
 */
class Roots_Nav_Walker extends Walker_Nav_Menu {
  function check_current($classes) {
    //return preg_match('/(current[-_])|active|nodropdown/', $classes);
    return preg_match('/(current[-_])|active/', $classes);
  }

  function start_lvl(&$output, $depth = 0, $args = array()) {
    
    $classes = '';
    if (preg_match_all( '<li class="([^"]+)">', $output, $matches )) {
      array_shift($matches[1]);
      $classes = array_pop($matches[1]);
    }
    
    if ($this->check_current($classes)) {
      $output .= "\n<ul class=\"nodropdown-menu collapse in\">\n";
    } else {
      $output .= "\n<ul class=\"nodropdown-menu collapse\">\n";
    }

  }

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    $item_html = '';
    parent::start_el($item_html, $item, $depth, $args);

    if ($item->is_dropdown && ($depth === 0)) {
      $collapse_target = sprintf('.menu-%s>ul', sanitize_title($item->title));
      $active = $this->check_current(implode('|',$item->classes));
      if ($active) {
        $item_html = str_replace('<a', sprintf('<a class="" data-toggle="collapse" data-target="%s"', $collapse_target), $item_html);
        $item_html = str_replace('</a>', ' <span class="dropup-reversed"><span class="caret"></span></span><span class="dropup"><span class="caret"></span></span></a>', $item_html);
        $item_html = str_replace('<a', '<a aria-expanded="true"', $item_html);
      } else {
        $item_html = str_replace('<a', sprintf('<a class="collapsed" data-toggle="collapse" data-target="%s"', $collapse_target), $item_html);
        $item_html = str_replace('</a>', ' <span class="dropup-reversed"><span class="caret"></span></span><span class="dropup"><span class="caret"></span></span></a>', $item_html);
        $item_html = str_replace('<a', '<a aria-expanded="false"', $item_html);
      }
    }
    elseif (stristr($item_html, 'li class="divider')) {
      $item_html = preg_replace('/<a[^>]*>.*?<\/a>/iU', '', $item_html);
    }
    elseif (stristr($item_html, 'li class="nodropdown-header')) {
      $item_html = preg_replace('/<a[^>]*>(.*)<\/a>/iU', '$1', $item_html);
    }

    $item_html = apply_filters('roots_wp_nav_menu_item', $item_html);
    $output .= $item_html;
  }

  function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
    $element->is_dropdown = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

    if ($element->is_dropdown) {
      $element->classes[] = 'nodropdown';
    }

    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }
}

/**
 * Remove the id="" on nav menu items
 * Return 'menu-slug' for nav menu classes
 */
function roots_nav_menu_css_class($classes, $item) {
  $slug = sanitize_title($item->title);
  $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
  $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

  $classes[] = 'menu-' . $slug;

  $classes = array_unique($classes);

  return array_filter($classes, 'is_element_empty');
}
add_filter('nav_menu_css_class', 'roots_nav_menu_css_class', 10, 2);
add_filter('nav_menu_item_id', '__return_null');

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Use Roots_Nav_Walker() by default
 */
function roots_nav_menu_args($args = '') {
  $roots_nav_menu_args['container'] = false;

  if (!$args['items_wrap']) {
    $roots_nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
  }

  if (current_theme_supports('bootstrap-top-navbar') && !$args['depth']) {
    $roots_nav_menu_args['depth'] = 2;
  }

  if (!$args['walker']) {
    $roots_nav_menu_args['walker'] = new Roots_Nav_Walker();
  }

  return array_merge($args, $roots_nav_menu_args);
}
add_filter('wp_nav_menu_args', 'roots_nav_menu_args');
