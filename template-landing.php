<?php
/*
Template Name: Landing Template
*/
?>

<?php get_template_part('templates/page', 'header'); ?>

<div class="landing-module-wrapper landing-module-wrapper--main">
	<div class="row">

		<div class="col-lg-8">
			<div class="slider-wrapper theme-sunyit">
				<div id="slider" class="nivoSlider">
					<?php if( $images = get_field('carousel') ): ?>
						<?php foreach( $images as $n => $image ): ?>
							<?php if ( $link = get_field('link',$image['id']) ): ?>
								<a href="<?php echo $link ?>"><img src="<?php echo $image['url'] ?>"></a>
							<?php else: ?>
								<img src="<?php echo $image['url'] ?>">
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
				<script type="text/javascript">
					$(window).load(function() {
							$('#slider').nivoSlider({
								effect: 'fade',
								pauseTime: 5000
							});
					});
				</script>
			</div>
		</div>
		<div class="col-lg-4">
		
			<div class="landing-module--links">
				<div class="landing-social-links">
					<ul>
						<li><a href="http://facebook.com/sunyit"><img src="<?php echo get_template_directory_uri() ?>/assets/img/social-icons-blue/facebookcircle.png"></a></li>
						<li><a href="http://twitter.com/sunyit"><img src="<?php echo get_template_directory_uri() ?>/assets/img/social-icons-blue/twittercircle.png"></a></li>
						<li><a href="http://youtube.com/sunyit"><img src="<?php echo get_template_directory_uri() ?>/assets/img/social-icons-blue/youtubecircle.png"></a></li>
						<li><a href="http://instagram.com/sunyit"><img src="<?php echo get_template_directory_uri() ?>/assets/img/social-icons-blue/instagramcircle.png"></a></li>
					</ul>
				</div>
				<nav class="landing-primary-links">
					<ul>
						<li><a href="/apply">Apply to SUNY Poly</a></li>
						<li><a href="/visit">Visit SUNY Poly</a></li>
						<li><a href="/programs/undergraduate">Learn More About Our Majors/Programs</a></li>
						<li><a href="/apps/form/?resource=admissions_request_moreinfo">Request More Information</a></li>
					</ul>
				</nav>
			</div>
									
		</div>

	</div>
</div>

<div class="row">

	<div class="col-lg-8 col-md-8 col-sm-12">
	
		<div class="landing-news landing-module landing-module--news-releases">

			<h2 class="h4 landing-module__heading section-header section-header">News</h2>

			<?php //do_shortcode('[feed url="http://www.sunyit.edu/apps/blogs/admissions/feed" num"2"]'); ?>
			<?php
			$feed = fetch_feed_with_options(array(
				'url' => get_field('news_feed'), //'http://www.sunyit.edu/apps/blogs/admissions/feed/',
				'num' => 2
			));

			foreach ($feed as $item): ?>
				<article class="summary">
					<h3 class="h4" class="summary__heading"><a href="<?php echo $item->get_link(); ?>"><?php echo shorten( $item->get_title(), 56); ?></a></h3>
					<p class="summary__content">
						<?php echo shorten( $item->get_content(), 340 ); ?>
						<span class="summary__more text-right"><strong><a href="<?php echo $item->get_link(); ?>">Read More</a> <span class="accent-color">&raquo;</span></strong></span>
					</p>
				</article>
			<?php endforeach; ?>
		
		</div>

		<?php if ($instagram_id = get_field('instagram_id')): ?>
		<div class="landing-module landing-module--instagram">
			<?php the_widget( 'null_instagram_widget', 'username='.$instagram_id.'&number=8' ); ?>
		</div>
		<?php endif; ?>
		
	</div>

	<div class="col-lg-4 col-md-4 col-sm-12">
		
		<?php if ($twitter_html = get_field('twitter_html')): ?>
			<div class="landing-module landing-module--twitter">
				<?php echo $twitter_html; ?>
			</div>
		<?php endif; ?>

		<?php if ($youtube_html = get_field('youtube_embed')): ?>
			<div class="landing-module landing-module--youtube">
				<?php echo $youtube_html; ?>
			</div>
		<?php endif; ?>

	</div>

</div>
