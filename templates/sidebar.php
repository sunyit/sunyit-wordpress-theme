<?php

  // TODO: separate markup from code
	if ($field = get_field('navigation_menus')):
	  foreach ($field as $menu):
	    echo '<section class="widget">';
        if (!empty( $menu['title'] )):
          echo '<h3>'.$menu['title'].'</h3>';
        endif;
        echo $menu['menu'];
	    echo '</section>';
	  endforeach;
	endif;

	if (get_field('hide_sidebar_widgets') !== TRUE):
		dynamic_sidebar('sidebar-primary');
	endif;
