<header class="banner" role="banner">

  <div class="container">

    <nav id="primary-navigation" class="navbar navbar-sunyit navbar-static-top yamm" role="navigation">
			
			<div class="navbar-header">
				<button class="navbar-btn btn btn-primary btn-outline btn-lg" data-toggle="collapse" data-target=".navbar-collapse">
					Menu
				</button>
				<a class="navbar-brand" href="/"><?php bloginfo('name'); ?></a>		
			</div>
    
      <div class="collapse navbar-collapse">

				<div class="navbar-sidebar">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>

        <?php
          if (has_nav_menu('primary_navigation')) :
            //wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
          endif;
        ?>
        
        <?php
        
          echo get_option('suny_poly_theme_menu_html');
        
        ?>

      </div>

    </nav>

  </div>

</header>
