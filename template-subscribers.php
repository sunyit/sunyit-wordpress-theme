<?php
/*
Template Name: Subscribers Only
*/
?>

<?php get_template_part('templates/page', 'header'); ?>

<?php if(is_user_logged_in()): ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php else: ?>
  <p class="alert alert-info">Please log in with your SITNET ID and password.</p>
  <p><a class="btn btn-primary btn-lg" href="<?php echo wp_login_url(); ?>?redirect_to=<?php echo $_SERVER['REQUEST_URI'] ?>">Log in</a></p>
<?php endif; ?>


